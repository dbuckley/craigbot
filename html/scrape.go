package html

import (
	"log"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

// Check that the given string is a URL
func check(s string) bool {
	_, err := url.ParseRequestURI(s)
	if err != nil {
		return false
	} else {
		return true
	}
}

// Iterates a tokenizer and returns slice of tokens
func tokens(z *html.Tokenizer) []html.Token {
	var t []html.Token
	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			// End of document
			break
		} else if tt == html.StartTagToken {
			t = append(t, z.Token())
		}
	}
	return t
}

func tokenLinks(z *html.Tokenizer) []string {
	var clnk []string

	tk := tokens(z)
	for _, t := range tk {
		var s []string
		for _, a := range t.Attr {
			if a.Key == "href" {
				s = append(s, a.Val)
			}
		}
		for _, l := range s {
			if strings.Contains(l, "html") {
				clnk = append(clnk, l)
			}
		}
	}
	return clnk
}

// Clean the slice by sorting then removing duplicates
//func clean([]string) []string {}

// Takes a URL as a string and returns all html links as a slice of string
func Links(s string) []string {
	var l []string
	if check(s) {
		resp, _ := http.Get(s)
		defer resp.Body.Close()

		z := html.NewTokenizer(resp.Body)

		l := tokenLinks(z)
		return l
	} else {
		log.Fatal("An improper URL was given")
		return l
	}
}
