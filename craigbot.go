package main

import (
	"fmt"
	"strings"

	"gitlab.com/dbuckley/craigbot/html"
)

func main() {
	s := "https://boston.craigslist.org/search/sss?query=tower+speakers&sort=rel"
	l := html.Links(s)
	for _, i := range l {
		fmt.Println(i)
	}
	fmt.Println(sliceMap(l))
}

// Create map of Craigslist ID and URL
func sliceMap(ss []string) map[string]string {
	var m = map[string]string{}
	for _, s := range ss {
		sp := strings.Split(s, "/")
		a := sp[len(sp)-1]
		b := strings.Split(a, ".")
		id := b[0]
		m[id] = s
	}
	return m
}
